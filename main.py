import os, traceback
import discord
import importlib
import gettext

from discord import app_commands
from discord.ext import commands
from peewee import SqliteDatabase, Model
from configparser import ConfigParser


class MyBot(commands.Bot):
    def __init__(self):
        """ Initialize the bot. """

        intents = discord.Intents.default()
        intents.message_content = True
        super().__init__("/", intents = intents)

        # Load the general config
        self.load_general_config()

        # Load localization
        gettext.translation("messages", localedir="locales", languages=[self.config["general"]["language"]]).install()

        # Load the plugins
        self.plugins = []
        for file in os.listdir("plugins"):
            self.plugins.append(importlib.import_module(f"plugins.{file}.plugin").Plugin())

        # Load plugins config
        self.load_plugins_config()

        # Load the database
        self.load_database()

        # Setup the plugins
        for plugin in self.plugins:
            plugin.setup(self)

        # Register the events
        for plugin in self.plugins:
            plugin.register_events(self)

    async def setup_hook(self):
        """ A hook that is called to setup the bot.

        This hook is called after the bot is logged in, but before
        it is connected to the websocket.
        """

        for plugin in self.plugins:
            plugin.register_commands(self.tree)
        await self.tree.sync()

    def load_general_config(self):
        """ Load the general config.

        This function should be called once while
        setting up the bot.
        And only after loading the plugins.
        """

        self.config = ConfigParser()
        # Load defaults before reading the file
        self.config["general"] = {
                "database_file": "config/database.db",
                "discord_bot_token": "<token>",
                "language": "en"
                }

        # Load config from file, overwriting any default
        self.config.read("config/config.ini")

        # Save it incase there are defaults that are not yet saved
        self.save_config()

    def load_plugins_config(self):
        """ Load the plugins config.

        This function should be called once while
        setting up the bot.
        And only after loading the plugins.
        """

        for plugin in self.plugins:
            plugin.load_defaults(self.config)

        # Load config from file, overwriting any default
        self.config.read("config/config.ini")

        # Save it incase there are defaults that are not yet saved
        self.save_config()


    def save_config(self):
        """ Save the config to the file.

        This function should be called when you have made changes
        to the config that should be flushed to the disk.
        """

        with open("config/config.ini", 'w+') as configfile:
            self.config.write(configfile)

    def load_database(self):
        """ Load the database.
        
        This function should be called after loading the plugins and config.
        """

        self.db = SqliteDatabase(self.config["general"]["database_file"])
        self.db.connect()

        models = Model.__subclasses__()
        with self.db:
            self.db.bind(models)
            self.db.create_tables(models)

    def start_bot(self):
        self.run(self.config["general"]["discord_bot_token"], reconnect = True)

# Start the bot
bot = MyBot()
bot.start_bot()
