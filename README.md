# Transporteur

## Getting started
1. Run `docker compose build`
2. Run `docker compose up` to generate the config files
3. Edit the config files in `config/`
4. Run `docker compose up` again to start the bot properly

## Add translation
Translations in Transporteur are handled by the GNU gettext utilities.

### Create translatable string in python
To create a translatable string in python, all you have to do is wrap the string in `_()`, for example:

```python
print(_("This string can be translated"))
```

### Update existing languages
The template file which contains all translatable string is located at `locales/transporteur.pot`
The translated strings for a language are located in `locales/<lang>/LC_MESSAGES/messages.po`

To add new strings to these files, run `update_messages.sh` in the `locales` directory.
This should update the `messages.po` which can then be edited.

### Add a new language
To add a new language, simply create the `locales/<lang>` directory and run `update_languages.sh`
