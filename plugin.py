import discord

from discord.ext import commands
from configparser import ConfigParser

class Plugin:
    """ An abstract class to define a single plugin. """

    def setup(self, bot: commands.Bot):
        self.bot = bot

    def load_defaults(self, config: ConfigParser):
        """ Load the default config settings. """
        pass

    def register_events(self, client: discord.Client):
        """ Used to register the events the plugin wants to listen to. """
        pass
    
    def register_commands(self, tree: discord.app_commands.CommandTree):
        """ Used to register the commands the plugin wants to use. """
        pass
