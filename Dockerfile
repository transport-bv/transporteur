# syntax=docker/dockerfile:1

FROM    ubuntu as locale_builder

WORKDIR /locales

RUN     rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache
RUN     --mount=type=cache,target=/var/cache/apt,sharing=locked \
        --mount=type=cache,target=/var/lib/apt,sharing=locked \
        apt-get update && \
        apt-get --no-install-recommends install -y gettext

COPY    locales/ .

RUN     ./generate_messages.sh

FROM    python:3.8-slim-buster as bot

WORKDIR /transporteur

COPY    requirements.txt requirements.txt
RUN     pip3 install -r requirements.txt

COPY    . .
COPY    --from=locale_builder locales locales

CMD     ["python3", "-u", "main.py"]
