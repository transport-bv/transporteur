#!/bin/bash

mydir="${0%/*}"

# Delete template file
rm $mydir/messages.pot 2> /dev/null

# for f in $(find $mydir/.. -name '*.py'); do
xgettext --no-location -o $mydir/messages.pot $(find $mydir/../ -name '*.py')

# Merge template with all languages
for d in $mydir/*/; do
    mkdir -p $d/LC_MESSAGES

    if [ ! -f $d/LC_MESSAGES/messages.po ]; then
        cp $mydir/messages.pot $d/LC_MESSAGES/messages.po
    fi

    msgmerge -U -N --backup=off $d/LC_MESSAGES/messages.po $mydir/messages.pot
done

