#!/bin/bash

mydir="${0%/*}"

for d in $mydir/*/; do
    msgfmt -o $d/LC_MESSAGES/messages.mo $d/LC_MESSAGES/messages.po
done
