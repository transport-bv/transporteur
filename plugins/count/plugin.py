import discord
import plugin
import re

from discord import app_commands
from discord.ext import commands
from .models import CountUser
from configparser import ConfigParser
from peewee import fn

# ========== Define plugin ==========

class Plugin(plugin.Plugin):

    # ========== Setup function ==========

    def setup(self, bot: commands.Bot):
        self.bot = bot
        self.enabled = bot.config["count"]["enabled"]
        self.is_active = False
        self.current_number = 0
        self.current_user = 0
        self.channel_id = int(bot.config["count"]["channel_id"])
        self.format = bot.config["count"]["format"]

    def load_defaults(self, config: ConfigParser):
        config["count"] = {
                "enabled": True,
                "channel_id": "<channel id>",
                "format": "(\d+)(?: - .*)?"
                }

    def register_events(self, client: discord.Client):
        if self.enabled:
            client.add_listener(self.on_ready)
            client.add_listener(self.on_message)

    def register_commands(self, tree: app_commands.CommandTree):
        if self.enabled:
            group = app_commands.Group(name=_("count_command"), description=_("count_group_command_description"))
            group.add_command(app_commands.Command(name=_("highscore_command_name"), description=_("highscore_command_description"), callback=self.highscore))
            group.add_command(app_commands.Command(name=_("success_top_command_name"), description=_("succes_top_command_description"), callback=self.success_top))
            group.add_command(app_commands.Command(name=_("failure_top_command_name"), description=_("failure_top_command_description"), callback=self.failure_top))
            tree.add_command(group)

    # ========== Define commands ==========

    async def highscore(self, interaction: discord.Interaction):
        # Collect correct answer
        highscore = CountUser.select(fn.MAX(CountUser.high_score)).scalar()
        user = CountUser.get(CountUser.high_score == highscore)

        # Check if we should be quiet in the counting channel
        isEphemeral = interaction.channel_id == self.channel_id

        await interaction.response.send_message(
                _("highscore_command_response").format(user=f"<@{user.user_id}>", highscore=highscore),
                ephemeral = isEphemeral,
                allowed_mentions = discord.AllowedMentions.none()
        )

    async def success_top(self, interaction: discord.Interaction):
        # Collect correct answer
        users = CountUser.select(CountUser).order_by(CountUser.success_count.desc()).limit(3)
        results = [(x.user_id, x.success_count) for x in users]
        message = _("success_top_command_response")

        for i, result in enumerate(results, 1):
            message += f"\n{i}) {result[1]} - <@{result[0]}>"

        # Check if we should be quiet in the counting channel
        isEphemeral = interaction.channel_id == self.channel_id

        await interaction.response.send_message(
                message,
                ephemeral = isEphemeral,
                allowed_mentions = discord.AllowedMentions.none()
        )

    async def failure_top(self, interaction: discord.Interaction):
        # Collect correct answer
        users = CountUser.select(CountUser).order_by(CountUser.failure_count.desc()).limit(3)
        results = [(x.user_id, x.failure_count) for x in users]
        message = _("failure_top_command_response")

        for i, result in enumerate(results, 1):
            message += f"\n{i}) {result[1]} - <@{result[0]}>"

        # Check if we should be quiet in the counting channel
        isEphemeral = interaction.channel_id == self.channel_id

        await interaction.response.send_message(
                message,
                ephemeral = isEphemeral,
                allowed_mentions = discord.AllowedMentions.none()
        )

    # ========== Helper functions ==========

    async def get_last_message(self, channel: discord.TextChannel) -> int:
        async for message in channel.history(limit=1):
            if (message.content.isdigit()):
                self.is_active = True
                self.current_number = int(message.content)
                self.current_user = message.author.id

    def is_correct_number(self, message: discord.Message) -> bool:
        try:
            m = re.search(self.format, message.content)
            if (m == None):
                return False

            if (int(m[1]) == self.current_number + 1):
                return True
        except:
            return False

    # ========== Event handlers ==========

    async def on_ready(self):
        await self.get_last_message(self.bot.get_channel(self.channel_id))

    async def on_message(self, message: discord.Message):
        if (message.author.id == self.bot.user.id):
            return

        if (message.channel.id == self.channel_id):
            correct_number = self.is_correct_number(message)

            # If game is not yet active, start it
            if not self.is_active and correct_number:
                self.is_active = True

            user, __ = CountUser.get_or_create(user_id = message.author.id)

            # If game is active, the same user can only message once
            if self.is_active and (self.current_user == message.author.id):
                self.is_active = False
                self.current_number = 0
                user.failure_count += 1
                self.current_user = 0
                await message.channel.send(_("count_fail_twice").format(user=f"<@{message.author.id}>"))

            # If game is active, handle correct or incorrect message
            elif self.is_active and correct_number:
                self.current_number += 1
                self.current_user = message.author.id
                user.success_count += 1
                if user.high_score < self.current_number:
                    user.high_score = self.current_number

            elif self.is_active and not correct_number:
                self.is_active = False
                await message.channel.send(_("count_fail_response").format(user=f"<@{message.author.id}>", expected=(self.current_number + 1)))
                self.current_number = 0
                self.current_user = 0
                user.failure_count += 1
            user.save()
